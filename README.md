# README #

# Managing Cloudformation Stacks using JSON

There is a rake task to manage CloudFormation (CF) stacks.  This rake (cfjson) accepts the JSON to create/update the stack and changeset. The definition files are named after the service, such as stores.rb and live in `cloudformation/definitions`.  You will also need to provide an `ami_id` that defines which ami will be used for instances that are created.

You will also need to provide your AWS credentials.  If you only have one set, then you are good to go and need to do nothing further.  Run `aws configure` to set your AWS credentials locally.  If you have more than one set, then you need to provide the profile using the `AWS_PROFILE` variable.

And then run this command to create a new stack:
```
bundle exec rake cfjson:create \
environment_name=integration \
file_name=upm \
ami_id=ami-6408f109 \
major_version=4 \
AWS_PROFILE=wfmold
```

You can also create a changeset for an existing stack with the following command:
```
bundle exec rake cfjson:changeset \
environment_name=integration \
file_name=upm \
major_version=4 \
ami_id=ami-6408f109 \
AWS_PROFILE=wfmold
```