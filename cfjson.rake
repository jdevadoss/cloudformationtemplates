require 'byebug'
require 'aws-sdk'
require 'pp'

# AWS client info
#
# Rake tasks:
# cfjson:create - create stack for the given json template
# cfjson:changeset - create a Cloudformation changeset for the given json template
# cfjson:update - update stack with provided json template

creds = Aws::Credentials.new(ENV['AWS_ACCESS_KEY_ID'], ENV['AWS_SECRET_ACCESS_KEY'])
@cloudformation = Aws::CloudFormation::Client.new(region:'us-east-1', credentials: creds)

namespace :cfjson do

  # Create
  task :create do
    raise 'Invalid environment variables. environment_name, file_name, and major_version are all required.' if params_invalid?

    validate_json(filename)

    puts "creating stack #{stack_name}..."
    response = @cloudformation.create_stack(
      stack_name: stack_name,
      template_body: File.open(filename, 'r').read,
      timeout_in_minutes: 50, # takes over 20 minutes just for the db, 40 for the the replica
      capabilities: ['CAPABILITY_IAM'],
      on_failure: 'DELETE'
    )
    pp response
  end

  task :changeset do
    validate_json(filename)

    now = Time.now.strftime('%Y%m%d%H%M%S')

    response = @cloudformation.create_change_set(
        stack_name: stack_name,
        template_body: File.open(filename, 'r').read,
        change_set_name: "#{stack_name}-#{now}",
        capabilities: ['CAPABILITY_IAM'],
        description: "Created #{now}"
    )
    puts "Created changeset: #{response['id']}"
    puts "for stack: #{response['stack_id']}"
  end

  task :watch do
    wait_until_stack_completes
  end

  task :update do
    raise 'Invalid environment variables. environment_name, file_name, major_version and ami_id are all required.' if params_invalid?

    validate_json(filename)

    puts "updating stack #{stack_name}..."
    # example https://github.com/arothian/inspec-aws/blob/master/examples/bin/demo_noncompliant_vpc.rb
    response = @cloudformation.update_stack(
        stack_name: stack_name,
        capabilities: ['CAPABILITY_IAM'],
        template_body: File.open(filename, 'r').read
    )
    pp response
  end

  def validate_json(json_file)
    contents = File.open(json_file, 'r').read
    response = @cloudformation.validate_template(template_body: contents)

    puts 'validating json file...'
    response
  end

  def wait_until_stack_completes
    status = @cloudformation.describe_stacks stack_name: stack_name
    sleep 10 until stack_finished?(status[0][0]['stack_status'])
  end

  def params_invalid?
    ENV['environment_name'].nil? &&
      ENV['service_name'].nil? &&
      ENV['major_version'].nil?
  end

  def filename
    # using underscores for filenames to appease rubocop
    ENV['file_name']
  end

  def expanded_stack_name
    # using underscores for filenames to appease rubocop
    "#{ENV['file_name']}_v#{ENV['major_version']}_#{ENV['environment_name']}"
  end

  def stack_name
    # using dashes for stack names to appease AWS
    expanded_stack_name.to_s.tr('_', '-')
  end
end